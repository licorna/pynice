# PyNice -- Please Python Be Nice.

A simple shell wrapper around the tedious task of creating and
maintaining different virtualenvs, and use the correct one to fire up
your favourite Python script.

## Quick Example

Let's assume you have this nice Python script that depends on an
installation of the nice `requests` library, so you'll have a
`requirements.txt` file like:

```
requests==2.19.1
```

And your simple Python script:


``` python
#!/usr/bin/env python

import requests

# check if pypi is reachable!
print(requests.get('https://pypi.org'))
```

Before you run this script you'll have to properly create a
virtualenv, activate, update (if necessary) and run:

    python -m venv venv
    source venv/bin/activate
    python -m pip install -r requirements.txt
    ./check_pypi_is_up
    # probably you'll want to deactivate it now

With `pynice` you only have to change the shebang of your script;
using the same example:

``` python
#!/usr/bin/env pynice

import requests

# check if pypi is reachable!
print(requests.get('https://pypi.org'))
```

From now on, you will only have to run your script:

    ./check_pypi_is_up

And it will, just work.

## Installation

You still want to give this thing a try? Fair enough, just clone this
repo and copy the `pynice` file to somewhere in your `${PATH}`. Make
sure the `pynice` script is made executable.

## How is this thing even possible?

The `#!` character combination is known as "shebang" or "hashbang"
(and others), and it is a interpreter directive. It informs the
Operating System, which interpreter program needs to be used to
execute this script. In our case, instead of using `python`, we
"execute" this script with `pynice` that will, before calling `python`
to execute the actual script, create a virtualenv and install all the
requirements there. If one virtualenv already exists, it will be used
and updated instead.

## Questions nobody asked me but here they are

### Does this monstrocity supports multiple Python versions?

Yes, as long as they are installed in the host. A `pynice` hashbang
looks something like:

    #!/usr/bin/env pynice

To specify a given interpreter you'll have to annotate the file as in
the following example:

    # pynice:interpreter=<python-interpreter>

Where `<python-interpreter>` can be any interpreter installed in the
host Operating System. I'm currently using `python` for Python
2.7. `python3` for Python 3.7 and `python3.6` for Python 3.6. This is
something I feel comfortable with, because 3.6 and 3.7 are my main
Python versions while 2.7 needs to be supported as well.

### Managing the installation of `virtualenv` and `pip`

Both `virtualenv` and `pip` need to be installed as modules for
`pynice` to work, for any given Python version. `pynice` will not
do this for you.

### Are the virtual environments reused?

Yes, they are installed in the same directory your scripts resides,
inside a `.venv` directory. Each script in a directory will have its
own virtual environment. Not ideal, but this is just a proof of concept.

### How to specify requirements.txt file

In case your requirements file is not called `requirements.txt` you
can use the `requirements` annotation in your Python file, like in the
following example:

    #!/usr/bin/env pynice
    # pynice:requirements=reqs.txt

In this case, dependencies will be installed from `reqs.txt` file, and
not `requirements.txt`.

## Full example

``` python
#!/usr/bin/env pynice
# pynice:requirements=reqs.txt
# pynice:interpreter=python3.6
#
import requests

# check if pypi is reachable!
print(requests.get('https://pypi.org'))
```
